import { beginCafeCreate_payload, beginEmployeeCreate_payload } from 'core/redux/actions/cafe';
import { beginCompositionCreate_payload, beginCompositionSearch_payload } from 'core/redux/actions/composition';
import { beginItemCreate_payload } from 'core/redux/actions/item';
import { beginWorkshiftCreate_payload } from 'core/redux/actions/workshift';
import { ICafeDTO, IUpdateCafeDTO } from 'core/redux/types/dto/cafe';
import { IEmployeeDTO } from 'core/redux/types/dto/employee';
import { IComposition, ICompositionAssign, IItemDTO } from 'core/redux/types/dto/item';
import { IOrder } from 'core/redux/types/dto/order';
import { IUserDTO } from 'core/redux/types/dto/user';
import { IWorkShiftDTO } from 'core/redux/types/dto/workshift';
import { removeEmptyFields } from 'core/utils/removeEmptyFields';
import getApi from './getApi';
import { employeeFields, generateCafeGetter, generateGetter } from './structures';
import { refreshToken } from './utils';

export enum ERequestTypes {
    GET = 'get',
    POST = 'post',
    PATCH = 'patch',
    DELETE = 'delete'
}

export interface IApi {
    refreshToken: () => any;

    authUser: (data: any) => any;

    registerUser: (data: {
        email: string
    }) => any;

    getUserData: () => IUserDTO;
    getOwnedCafes: () => ICafeDTO[];
    getCafeByID: (ID: string) => ICafeDTO;
    getCafeOrders: ({id}: {id: string}) => IOrder[];

    createCafe: (cafeData: beginCafeCreate_payload) => ICafeDTO;
    updateCafe: ({id, newData}: {id: string, newData: IUpdateCafeDTO}) => boolean;

    createEmployee: ({cafeID, employeeData}: {cafeID: string, employeeData: beginEmployeeCreate_payload}) => IEmployeeDTO;
    updateEmployee: ({id, newData}: {id: string, newData: any}) => boolean;

    createWorkshift: ({cafeID, workshiftData}: {cafeID: string, workshiftData: any}) => IWorkShiftDTO;
    deleteWorkshift: ({id}: {id: string}) => boolean;
    assignWorkshift: ({id, employeeID}: {id: string, employeeID: string}) => boolean;
    unassignWorkshift: ({id, employeeID}: {id: string, employeeID: string}) => boolean;

    updateItem: ({id, newData}: {id: string, newData: any}) => boolean;
    createItem: (data: beginItemCreate_payload) => IItemDTO;

    findCompositions: (data: beginCompositionSearch_payload) => IComposition[];
    createComposition: ({data} : {data: beginCompositionCreate_payload}) => IComposition;
}

const api: IApi = getApi({
    refreshToken: {
        requestHandler: refreshToken
    },
    authUser: {
        path: () => '/auth/login',
        type: ERequestTypes.POST
    },

    registerUser: {
        path: () => '/auth/register',
        type: ERequestTypes.POST
    },

    getUserData: {
        path: () => '/user/self',
        type: ERequestTypes.GET
    },

    getOwnedCafes: {
        path: () => '/cafe/owned',
        type: ERequestTypes.GET
    },

    getCafeByID: {
        path: (ID: string) => `/cafe/${ID}`,
        type: ERequestTypes.GET
    },

    getCafeOrders: {
        path: ({id}) => `/order/cafe/${id}`,
        type: ERequestTypes.GET
    },

    createCafe: {
        path: () => '/cafe',
        type: ERequestTypes.POST
    },

    updateCafe: {
        path: ({ id }) => `/cafe/${id}`,
        dataHandler: ({ newData }) => ({
            ...newData
        }),
        type: ERequestTypes.PATCH
    },

    updateEmployee: {
        path: ({ id }) => `/employee/${id}`,
        dataHandler: ({ newData }) => ({
            ...newData
        }),
        type: ERequestTypes.PATCH
    },


    createEmployee: {
        path: () => '/employee',
        dataHandler: ({ employeeData, cafeID }) => ({
            cafeID,
            ...employeeData,
        }),
        type: ERequestTypes.POST
    },

    createItem: {
        path: () => '/item',
        dataHandler: (data) => ({
            ...data,
        }),
        type: ERequestTypes.POST
    },

    createWorkshift: {
        path: () => '/workshift',
        dataHandler: ({ workshiftData, cafeID }) => ({
            cafeID,
            ...workshiftData,
        }),
        type: ERequestTypes.POST
    },

    updateWorkshift: {
        path: ({ id }) => `/workshift/${id}`,
        dataHandler: ({ newData }) => ({
            ...newData
        }),
        type: ERequestTypes.PATCH
    },

    updateItem: {
        path: ({ id }) => `/item/${id}`,
        dataHandler: ({ newData }) => ({
            ...newData,
            composition: newData.composition.filter((item: ICompositionAssign) => item.mass > 0)
        }),
        type: ERequestTypes.PATCH
    },

    assignWorkshift: {
        path: ({ id }) => `/workshift/${id}/employee`,
        dataHandler: ({ employeeID }) => ({
            employeeID
        }),
        type: ERequestTypes.POST
    },

    unassignWorkshift: {
        path: ({ id }) => `/workshift/${id}/employee`,
        dataHandler: ({ employeeID }) => ({
            employeeID
        }),
        type: ERequestTypes.DELETE
    },

    deleteWorkshift: {
        path: ({ id }) => `/workshift/${id}`,
        type: ERequestTypes.DELETE
    },

    findCompositions: {
        path: () => `/composition/search`,
        type: ERequestTypes.POST
    },

    createComposition: {
        path: () => '/composition',
        type: ERequestTypes.POST,
        dataHandler: ({ data }) => ({
            ...data,
            pfc: {
                proteins: parseFloat(data.pfc.proteins.replace(',','.').replace(' ','')),
                fats: parseFloat(data.pfc.fats.replace(',','.').replace(' ','')),
                carbohydrates: parseFloat(data.pfc.carbohydrates.replace(',','.').replace(' ',''))
            }
        }),
    }
});

export default api;