import { createAction } from "redux-actions";
import { ICafeDTO, IUpdateCafeDTO } from "../types/dto/cafe";
import { IEmployeeDTO } from "../types/dto/employee";
import { IOrder } from "../types/dto/order";

export const getCafeDataById = createAction<string>(
  'GET_CAFE_DATA_BY_ID'
);

export const getCafeOrders = createAction<string>(
  'GET_CAFE_ORDERS'
);
export const successGetCafeOrders = createAction<IOrder[]>(
  'SUCCESS_GET_CAFE_ORDERS'
);


export type beginCafeCreate_payload = {
  name: string;
  address: string;
};
export const beginCafeCreate = createAction<beginCafeCreate_payload>(
  'BEGIN_CAFE_CREATE'
);
export const successCafeCreate = createAction<ICafeDTO>(
  'SUCCESS_CAFE_CREATE'
);
export const failCafeCreate = createAction<string>(
  'FAIL_CAFE_CREATE'
);

export const setSelectedCafeID = createAction<string | null>(
  'SET_SELECTED_CAFE_ID'
);


export type beginCafeUpdate_payload = {
  id: string;
  newData: Partial<ICafeDTO>
};
export const beginCafeUpdate = createAction<beginCafeUpdate_payload>(
  'BEGIN_CAFE_CLOSE'
);
export const failCafeUpdate = createAction(
  'FAIL_CAFE_CLOSE'
);

export type successCafeUpdate_payload = {
  id: string;
  newData: Partial<ICafeDTO>;
};
export const successCafeUpdate = createAction<successCafeUpdate_payload>(
  'SUCCESS_CAFE_UPDATE'
);



export type beginEmployeeCreate_payload = {
  first_name: string;
  last_name: string;
  position: string;
};
export const beginEmployeeCreate = createAction<beginEmployeeCreate_payload>(
  'BEGIN_EMPLOYEE_CREATE'
);
export const successEmployeeCreate = createAction<IEmployeeDTO>(
  'SUCCESS_EMPLOYEE_CREATE'
);
export const failEmployeeCreate = createAction<string>(
  'FAIL_EMPLOYEE_CREATE'
);


export type beginEmployeeUpdate_payload = {
  id: string;
  newData: Partial<IEmployeeDTO>
};
export const beginEmployeeUpdate = createAction<beginEmployeeUpdate_payload>(
  'BEGIN_EMPLOYEE_UPDATE'
);
export const failEmployeeUpdate = createAction(
  'FAIL_EMPLOYEE_UPDATE'
);