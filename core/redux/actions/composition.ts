import { createAction } from "redux-actions";
import { IEmployeeDTO } from "../types/dto/employee";
import { IComposition, IPFC } from "../types/dto/item";


export type beginCompositionCreate_payload = {
  name: number;
  pfc?: IPFC;
  itemID: string;
};
export const beginCompositionCreate = createAction<beginCompositionCreate_payload>(
  'BEGIN_COMPOSITION_CREATE'
);
export const successCompositionCreate = createAction<IComposition>(
  'SUCCESS_COMPOSITION_CREATE'
);
export const failCompositionCreate = createAction<string>(
  'FAIL_COMPOSITION_CREATE'
);

export type beginCompositionSearch_payload = {
  searchText: number;
};
export const beginCompositionSearch = createAction<beginCompositionSearch_payload>(
  'BEGIN_COMPOSITION_SEARCH'
);
export const successCompositionSearch = createAction<IComposition[]>(
  'SUCCESS_GLOBAL_COMPOSITION_READ'
);

