import { createAction } from "redux-actions";
import { IEmployeeDTO } from "../types/dto/employee";
import { IWorkShiftDTO } from "../types/dto/workshift";


export type beginWorkshiftCreate_payload = {
  beginTime: number;
  endTime: number;
  dayOfWeek: number;
  cafeID: string;
};
export const beginWorkshiftCreate = createAction<beginWorkshiftCreate_payload>(
  'BEGIN_WORKSHIFT_CREATE'
);
export const successWorkshiftCreate = createAction<IWorkShiftDTO>(
  'SUCCESS_WORKSHIFT_CREATE'
);
export const failWorkshiftCreate = createAction<string>(
  'FAIL_WORKSHIFT_CREATE'
);

export const beginWorkshiftDelelte = createAction<string>(
  'BEGIN_WORKSHIFT_DELETE'
);
export const failWorkshiftDelelte = createAction(
  'FAIL_WORKSHIFT_DELETE'
);

export type beginEmployeeAssign_payload = {
  id: string;
  employeeID: string;
  value: boolean;
};
export const beginWorkshiftAssign = createAction<beginEmployeeAssign_payload>(
  'BEGIN_WORKSHIFT_ASSIGN'
);