import { createSelector } from '@reduxjs/toolkit';
import { handleActions, Action } from 'redux-actions';
import { setSelectedCafeID, successCafeUpdate, successCafeUpdate_payload, successEmployeeCreate } from '../actions/cafe';
import { successCompositionSearch } from '../actions/composition';

import { setOwnedCafes, setUserData } from '../actions/user';

import type { ICafeDTO } from '../types/dto/cafe';
import { IEmployeeDTO } from '../types/dto/employee';
import { IComposition } from '../types/dto/item';
import type { IUserDTO } from '../types/dto/user';

export interface ICompostionState {
  global?: IComposition[];
  searched?: IComposition[];
}

const initialState: ICompostionState = {
  global: undefined,
  searched: undefined
};

// TODO: remove all any
const reducer = handleActions<ICompostionState, any>({
  [successCompositionSearch.toString()]: (state, {payload}: Action<IComposition[]>) => ({
    ...state,
    searched: payload
  }),
}, initialState);

export default reducer;
