import { ReducersMapObject } from 'redux';

import auth, { IAuthState } from './auth';
import composition, { ICompostionState } from './composition';
import user, { IUsertate } from './user';

export interface IRootReducer{
    auth: IAuthState,
    user: IUsertate,
    composition: ICompostionState
}

const reducers: ReducersMapObject<IRootReducer, any> = {
    auth,
    user,
    composition
};

export default reducers;
