import { createSelector } from '@reduxjs/toolkit';
import { handleActions, Action } from 'redux-actions';
import { setSelectedCafeID, successCafeUpdate, successCafeUpdate_payload, successEmployeeCreate, successGetCafeOrders } from '../actions/cafe';
import { successCompositionCreate } from '../actions/composition';

import { setOwnedCafes, setUserData } from '../actions/user';

import type { ICafeDTO } from '../types/dto/cafe';
import { IEmployeeDTO } from '../types/dto/employee';
import { IComposition } from '../types/dto/item';
import { IOrder } from '../types/dto/order';
import type { IUserDTO } from '../types/dto/user';

export interface IUsertate {
  userData?: IUserDTO;
  ownedCafes?: ICafeDTO[];
  selectedCafeID?: string | null;
  cafeOrders: IOrder[];
}

const initialState: IUsertate = {
  userData: undefined,
  ownedCafes: undefined,
  selectedCafeID: typeof window !== 'undefined' ? window.localStorage?.getItem('cafeID') || undefined : undefined,
  cafeOrders: []
};

// TODO: remove all any
const reducer = handleActions<IUsertate, any>({
  [setUserData.toString()]: (state, {payload}: Action<IUserDTO>) => ({
    ...state,
    userData: payload
  }),
  
  [setOwnedCafes.toString()]: (state, {payload}: Action<ICafeDTO[]>) => ({
    ...state,
    ownedCafes: payload
  }),

  [successCafeUpdate.toString()]: (state, {payload}: Action<successCafeUpdate_payload>) => ({
    ...state,
    ownedCafes: state.ownedCafes?.map((cafe) => {
      if(cafe._id === payload.id) {
        const updCafe = {...cafe, ...payload.newData};
        return updCafe;
      }
      return cafe;
    })
  }),

  [setSelectedCafeID.toString()]: (state, {payload}: Action<string>) => ({
    ...state,
    selectedCafeID: payload
  }),

  [successGetCafeOrders.toString()]: (state, {payload}: Action<IOrder[]>) => ({
    ...state,
    cafeOrders: payload
  }),
}, initialState);

export default reducer;
