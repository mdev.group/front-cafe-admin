import { takeEvery, put, select, call, debounce, all, take, spawn } from 'redux-saga/effects'
import api from '../../api';

import {
  getUserData as getUserDataAction, setOwnedCafes, setUserData,
} from '../actions/user';
import { requestError } from '../actions/common';
import { waitForAuth } from './common';
import type { ICafeDTO } from '../types/dto/cafe';
import { beginCafeCreate, beginCafeCreate_payload,
  beginCafeUpdate_payload, beginCafeUpdate,
  beginEmployeeCreate, beginEmployeeCreate_payload,
  failCafeCreate, failEmployeeCreate, setSelectedCafeID,
  successCafeCreate, successCafeUpdate,
  successEmployeeCreate, failCafeUpdate, getCafeOrders, successGetCafeOrders } from '../actions/cafe';
import { PayloadAction } from '@reduxjs/toolkit';
import { IRootReducer } from '../reducers';
import { IEmployeeDTO } from '../types/dto/employee';
import { IOrder } from '../types/dto/order';

function* createCafe({payload}: PayloadAction<beginCafeCreate_payload>): Generator {
  try {
    yield call(waitForAuth);
    const response = (yield call(api.createCafe, payload)) as ICafeDTO;
    
    yield put(getUserDataAction());
    yield put(successCafeCreate(response));
  } catch (error) {
    yield put(failCafeCreate('Ошибка создания заведения'));
      yield put(requestError(error));
  }
}


function* updateCafe({payload}: PayloadAction<beginCafeUpdate_payload>): Generator {
  try {
    yield call(waitForAuth);

    const response = yield call(api.updateCafe, {
      id: payload.id,
      newData: payload.newData
    });
    
    yield put(successCafeUpdate({
      id: payload.id,
      newData: payload.newData
    }));

    yield call(getOwnedCafes);

  } catch (error) {
    yield put(requestError(error));
    yield put(failCafeUpdate('Ошибка обновления заведения'));
  }
}

function* getOwnedCafes(): Generator {
  try {
    yield call(waitForAuth);
    const cafesList = (yield call(api.getOwnedCafes)) as ICafeDTO[];
    yield put(setOwnedCafes(cafesList));

    yield call(selectFirstOpenedCafe, cafesList);

  } catch (error) {
      yield put(requestError(error));
  }
}

function* selectFirstOpenedCafe(cafesList: ICafeDTO[]): Generator {
  try {
    const selectedCafeID = (yield select((store: IRootReducer) => store.user.selectedCafeID)) as string | null | undefined;
    const firstOpenedCafe = cafesList.find((cafe)=>!cafe.closed) || null

    if(firstOpenedCafe !== null) {
      if(!selectedCafeID) {
        if(firstOpenedCafe) {
          yield put(setSelectedCafeID(firstOpenedCafe._id));
        } else {
          yield put(setSelectedCafeID(null));
        }
      }
    } else {
      yield put(setSelectedCafeID(null));
    }

  } catch (error) {
      yield put(requestError(error));
  }
}


function* createEmployee({payload}: PayloadAction<beginEmployeeCreate_payload>): Generator {
  try {
    yield call(waitForAuth);
    const selectedCafeID = (yield select((store: IRootReducer) => store.user.selectedCafeID)) as string | null | undefined;

    if(selectedCafeID) {
      const response = (yield call(api.createEmployee, {
        employeeData: payload,
        cafeID: selectedCafeID
      })) as IEmployeeDTO;
      
      yield put(successEmployeeCreate(response));
      yield put(getUserDataAction());
    } else {
      yield put(failEmployeeCreate('Ошибка добавления сотрудника. Заведение не выбрано.'));
    }
  } catch (error) {
    yield put(failEmployeeCreate('Ошибка добавления сотрудника'));
    yield put(requestError(error));
  }
}

function* saveSelectedCafeID({payload}: PayloadAction<string>): Generator {
  try {
    window.localStorage.setItem('cafeID', payload)
  } catch (error) {
    yield put(failEmployeeCreate('Ошибка добавления сотрудника'));
    yield put(requestError(error));
  }
}

function* getOrders({payload}: PayloadAction<string>): Generator {
  try {
    const response = (yield call(api.getCafeOrders, {
      id: payload
    })) as IOrder[];
    
    yield put(successGetCafeOrders(response));
  } catch (error) {
    yield put(requestError(error));
  }
}

export function* initCafeWatchers(){
  yield takeEvery(beginCafeCreate.toString(), createCafe);
  yield takeEvery(beginCafeUpdate.toString(), updateCafe);
  yield takeEvery(getUserDataAction.toString(), getOwnedCafes);
  yield takeEvery(beginEmployeeCreate.toString(), createEmployee);
  yield takeEvery(setSelectedCafeID.toString(), saveSelectedCafeID);
  yield takeEvery(getCafeOrders.toString(), getOrders);
}