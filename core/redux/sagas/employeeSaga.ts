import { takeEvery, put, call } from 'redux-saga/effects'
import api from '../../api';

import {
  getUserData as getUserDataAction
} from '../actions/user';
import { requestError } from '../actions/common';
import { waitForAuth } from './common';
import { failCafeUpdate, beginEmployeeUpdate, beginEmployeeUpdate_payload } from '../actions/cafe';
import { PayloadAction } from '@reduxjs/toolkit';


function* updateEmployee({payload}: PayloadAction<beginEmployeeUpdate_payload>): Generator {
  try {
    yield call(waitForAuth);

    const response = yield call(api.updateEmployee, {
      id: payload.id,
      newData: payload.newData
    });

    yield put(getUserDataAction());

  } catch (error) {
    yield put(requestError(error));
    yield put(failCafeUpdate('Ошибка обновления сотрудника'));
  }
}

export function* initEmployeeWatchers(){
  yield takeEvery(beginEmployeeUpdate.toString(), updateEmployee);
}