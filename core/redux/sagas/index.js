import { call, spawn, all } from 'redux-saga/effects'

import { initAuthWatchers } from './authSaga';
import { initCafeWatchers } from './cafeSaga';
import { initEmployeeWatchers } from './employeeSaga';
import { initItemWatchers } from './itemSaga';
import { initUserWatchers } from './userSaga';
import { initWorkshiftWatchers } from './workshiftSaga';

export default function* rootSaga(){
  const sagas = [initAuthWatchers, initUserWatchers, initCafeWatchers, initEmployeeWatchers, initWorkshiftWatchers, initItemWatchers];
  
  const retrySagas = sagas.map(saga=>{
    return spawn(function* (){
      while(true){
        try{
          yield call(saga);
          break;
        } catch (e) {
          console.error(e)
        }
      }
    })
  })

  yield all(retrySagas)
}