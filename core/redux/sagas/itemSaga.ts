import { takeEvery, put, select, call, debounce, all, take, spawn } from 'redux-saga/effects'
import api from '../../api';

import {
  getUserData as getUserDataAction, setOwnedCafes, setUserData,
} from '../actions/user';
import { requestError } from '../actions/common';
import { waitForAuth } from './common';
import { PayloadAction } from '@reduxjs/toolkit';
import { beginEmployeeAssign_payload, beginWorkshiftAssign, beginWorkshiftCreate, beginWorkshiftCreate_payload, beginWorkshiftDelelte, failWorkshiftCreate } from '../actions/workshift';
import { beginItemCreate, beginItemCreate_payload, beginItemUpdate, beginItemUpdate_payload } from '../actions/item';
import { beginCompositionCreate, beginCompositionCreate_payload, beginCompositionSearch, beginCompositionSearch_payload, successCompositionSearch } from '../actions/composition';
import { IComposition, IItemDTO } from '../types/dto/item';
import { getItemFromSelectedCafe_selector } from '../selectors';


function* create({payload}: PayloadAction<beginItemCreate_payload>): Generator {
  try {
    yield call(waitForAuth);

    yield call(api.createItem, payload);

    yield put(getUserDataAction());

  } catch (error) {
    yield put(requestError(error));
    yield put(failWorkshiftCreate('Ошибка создания позиции'));
  }
}

function* update({payload}: PayloadAction<beginItemUpdate_payload>): Generator {
  try {
    yield call(waitForAuth);

    yield call(api.updateItem, {
      id: payload.id,
      newData: payload.newData
    });

    yield put(getUserDataAction());

  } catch (error) {
    yield put(requestError(error));
    yield put(failWorkshiftCreate('Ошибка создания смены'));
  }
}

function* createComposition({payload}: PayloadAction<beginCompositionCreate_payload>): Generator {
  try {
    yield call(waitForAuth);

    const res: IComposition = (yield call(api.createComposition, {
      data: payload
    })) as IComposition;

    const item = (yield select(getItemFromSelectedCafe_selector(payload.itemID))) as IItemDTO

    console.log(item)

    yield put(beginItemUpdate({
      id: payload.itemID,
      newData: {
        composition: [...item.composition, {
          id: res._id,
          mass: 100,
          isEditable: false
        }]
      }
    }))

  } catch (error) {
    console.error(error); 
    yield put(requestError(error));
    yield put(failWorkshiftCreate('Ошибка создания смены'));
  }
}

function* findComposition({payload}: PayloadAction<beginCompositionSearch_payload>): Generator {
  try {
    yield call(waitForAuth);

    const res: IComposition[] = (yield call(api.findCompositions, payload)) as any;

    yield put(successCompositionSearch(res))

  } catch (error) {
    yield put(requestError(error));
    yield put(failWorkshiftCreate('Ошибка создания смены'));
  }
}

export function* initItemWatchers(){
  yield takeEvery(beginCompositionCreate.toString(), createComposition);
  yield takeEvery(beginItemUpdate.toString(), update);
  yield takeEvery(beginCompositionSearch.toString(), findComposition);
  yield takeEvery(beginItemCreate.toString(), create);
}