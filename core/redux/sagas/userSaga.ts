import redirectTo from 'core/utils/redirectTo';
import { takeEvery, put, select, call, debounce, all, take, spawn } from 'redux-saga/effects'
import { v4 as uuidv4 } from 'uuid';
import api from '../../api';
import authTokenService from '../../services/authTokenService';

import {
  getUserData as getUserDataAction, setOwnedCafes, setUserData,
} from '../actions/user';
import { requestError } from '../actions/common';
import { waitForAuth } from './common';
import type { IUserDTO } from '../types/dto/user';
import type { ICafeDTO } from '../types/dto/cafe';

function* getUserData(): Generator {
  try {
    yield call(waitForAuth);
    
    const response = (yield call(api.getUserData)) as IUserDTO;
    yield put(setUserData(response));
  } catch (error) {
      yield put(requestError(error));
  }
}


export function* initUserWatchers(){
  yield takeEvery(getUserDataAction.toString(), getUserData);
}