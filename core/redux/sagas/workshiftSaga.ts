import { takeEvery, put, select, call, debounce, all, take, spawn } from 'redux-saga/effects'
import api from '../../api';

import {
  getUserData as getUserDataAction, setOwnedCafes, setUserData,
} from '../actions/user';
import { requestError } from '../actions/common';
import { waitForAuth } from './common';
import { PayloadAction } from '@reduxjs/toolkit';
import { beginEmployeeAssign_payload, beginWorkshiftAssign, beginWorkshiftCreate, beginWorkshiftCreate_payload, beginWorkshiftDelelte, failWorkshiftCreate } from '../actions/workshift';


function* create({payload}: PayloadAction<beginWorkshiftCreate_payload>): Generator {
  try {
    yield call(waitForAuth);

    const {cafeID, ...data} = payload;

    const response = yield call(api.createWorkshift, {
      cafeID: cafeID,
      workshiftData: data
    });

    yield put(getUserDataAction());

  } catch (error) {
    yield put(requestError(error));
    yield put(failWorkshiftCreate('Ошибка создания смены'));
  }
}

function* assign({payload}: PayloadAction<beginEmployeeAssign_payload>): Generator {
  try {
    yield call(waitForAuth);

    const {value, ...data} = payload;

    if (value) {
      yield call(api.assignWorkshift, data);
    } else {
      yield call(api.unassignWorkshift, data);
    }

    yield put(getUserDataAction());

  } catch (error) {
    yield put(requestError(error));
    yield put(failWorkshiftCreate('Ошибка создания смены'));
  }
}

function* deleteshift({payload}: PayloadAction<string>): Generator {
  try {
    yield call(waitForAuth);

    yield call(api.deleteWorkshift, {
      id: payload
    });

    yield put(getUserDataAction());

  } catch (error) {
    yield put(requestError(error));
    yield put(failWorkshiftCreate('Ошибка создания смены'));
  }
}

export function* initWorkshiftWatchers(){
  yield takeEvery(beginWorkshiftCreate.toString(), create);
  yield takeEvery(beginWorkshiftAssign.toString(), assign);
  yield takeEvery(beginWorkshiftDelelte.toString(), deleteshift);
}