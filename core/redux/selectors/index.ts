import { createSelector } from "@reduxjs/toolkit"
import { IRootReducer } from "../reducers"

export const selectedCafeID_selector = (state: IRootReducer) => state.user.selectedCafeID;
export const ownedCafes_selector = (state: IRootReducer) => state.user.ownedCafes;
export const cafeOrders_selector = (state: IRootReducer) => state.user.cafeOrders;
export const selectedCafe_selector = createSelector([ownedCafes_selector, selectedCafeID_selector], (ownedCafes, currID) => ownedCafes?.find((item) => item._id === currID) || null)

export const globalCompositions_selector = (state: IRootReducer) => state.composition.global;
export const searchedCompositions_selector = (state: IRootReducer) => state.composition.searched;

export const getItemFromSelectedCafe_selector = (itemID: string) => createSelector([selectedCafe_selector], (selectedCafe) => selectedCafe?.items.find(item => item._id === itemID) || null)
