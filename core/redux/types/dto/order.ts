export enum EOrderStatus {
  form,
  created,
  approved,
  serve,
  checktime,
  loyalty,
  scoresChoose,
  paySelection,
  paytime,
  done
}

export interface IOrderItemIngredients {
  id: string;
  count: number;
  name: string;
  defaultCount: number;
  price: number;
  isEditable: boolean;
  massStep?: number;
}

export interface IOrderPosition {
  itemID: string;
  orderTime: number;
  ingredients: IOrderItemIngredients[]
  
  uuid: string;
  fixed: boolean;
}

export interface IOrder {
  _id: string;
  number: string;
  cafeID: string;
  positions: IOrderPosition[]

  // Время создания заказа
  beginTime: number;
  // Врямя принятия заказа
  pickedTime: number;
  // Время закрытия заказа
  endTime: number;
  // Время для готовности заказа
  takeTime: number;

  employeeID: string;
  table: string;
  payByCard: number;
  payByPaper: number;
  payByScores: number;
  status: EOrderStatus;
  clientID: string;
  loyaltyUsage: number;
  precheck?: boolean;
}