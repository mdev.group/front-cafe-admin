import type { ICafeDTO } from "./cafe";
import type { IEmployeeDTO } from "./employee";

export interface IWorkShiftDTO {
  _id: string;
  dayOfWeek: number;
  beginTime: number;
  endTime: number;
  employeeIDs: string[]
}