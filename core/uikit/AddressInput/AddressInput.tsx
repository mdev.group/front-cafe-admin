import { useCallback, useState } from 'react';
import { Map, Marker } from 'yandex-map-react';

import styles from './AddressInput.module.scss';

interface IProps {
  onChange: (value: {
    lat: string;
    lng: string;
  }) => void;
  value: {
    lat: string;
    lng: string;
  };
}

const AddressInput: React.FC<IProps> = ({
  onChange,
  value
}) => {
  const [MapApi, setApi] = useState<any>(null)
  const handleClick = useCallback((e) => {
    if (MapApi) {
      onChange({
        lat: e.get('coords')[0],
        lng: e.get('coords')[1]
      })
    }
  }, [MapApi, onChange])

  const onAPIAvailable = useCallback((e) => {
    setApi(e);
  }, [])

  return (
    <div className={styles.map}>
      <Map
        onAPIAvailable={onAPIAvailable}
        center={[Number(value?.lat) || 55.754734, Number(value?.lng) || 37.583314]}
        zoom={Number(value?.lat) ? 16 : 10}
        onClick={handleClick}
        height={240}
        width={'100%'}
      >
        {value && value?.lat && value?.lng && (
          <Marker lat={Number(value.lat)} lon={Number(value.lng)} />
        )}
      </Map>
    </div>
  )
}

export default AddressInput;