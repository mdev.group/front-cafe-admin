import { CSSProperties } from 'react';
import styles from './Grid.module.scss'

interface IProps {
    itemSize?: string;
    gap?: string;
}

const Grid: React.FC<IProps> = ({
    children,
    itemSize,
    gap = '8px'
})=>{
    return (
        <div className={styles.grid} style={{'--itemSize': itemSize, gap} as CSSProperties}>
            {children}
        </div>
    )
}

export default Grid;