import classNames from 'classnames';
import { useCallback, useEffect, useRef } from 'react';
import styles from './Image.module.scss';

/* eslint-disable @next/next/no-img-element */
interface IProps {
  src: string;
  alt?: string;
  className?: string;
  placeholderUrl?: string;
}

const Image: React.FC<IProps> = ({
  src,
  alt = "",
  className,
  placeholderUrl = '',
})=>{
  const ref = useRef<HTMLImageElement>(null);

  const onError = useCallback(({currentTarget}) => {
      currentTarget.src = placeholderUrl;
  }, [placeholderUrl])

  useEffect(() => {
    if(ref.current && ref.current.src !== src) {
      ref.current.src = src;
    }
  }, [src, ref])

  return (
    <>
        {src || placeholderUrl ? (
          <img
          ref={ref}
          src={src}
          alt={alt}
          className={classNames(
            className,
            styles.image
          )}
          onError={onError}/>
        ) : (
          <div className={classNames(
            className,
            styles.image
          )}/>
        )}
    </>
  )
}

export default Image;