import styles from "./ModalForm.module.scss";

const Userimage: React.FC = ({
  children,
}) => { 
  return (
    <div className={styles.modalForm}>
      {children}
    </div>
  )
}

export default Userimage;