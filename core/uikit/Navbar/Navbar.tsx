import { useCallback, useEffect, useMemo, useState } from "react";
import Image from "core/uikit/Image";
import Navitem from "./components/Navitem";

import { navigation } from "./constants";

import styles from "./Navbar.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { IRootReducer } from "core/redux/reducers";
import redirectTo from "core/utils/redirectTo";
import { getUserData } from "core/redux/actions/user";
import authTokenService from "core/services/authTokenService";
import Userimage from "../Userimage";
import type { IUserDTO } from "core/redux/types/dto/user";
import CafeSelector from "./components/CafeSelector";
import { useRouter } from "next/router";
import { selectedCafe_selector } from "core/redux/selectors";
import classNames from "classnames";

const Navbar: React.FC = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const userdata = useSelector<IRootReducer, IUserDTO | undefined>((store)=>store.user.userData);
  const selectedCafe = useSelector(selectedCafe_selector);

  useEffect(()=>{
    if(!authTokenService.hasTokenInLocalstorage()) {
      redirectTo('/auth');
    }
    dispatch(getUserData());
  }, [dispatch])

  const handleGoToAccount = ()=>{
    redirectTo('/auth/user');
  }

  const isLoaded = useMemo(()=>{
    return [userdata].every(item=>item !== undefined);
  }, [userdata])

  const [opened, setOpened] = useState(false);

  return (
    <>
      <button className={styles.navBtn} onClick={() => setOpened(!opened)}>
        <Image
          src={opened ? "/images/icons/cross.svg" : "/images/icons/nav.svg"}
          alt=""
        />
      </button>
      <nav className={classNames(styles.navbar, {
        [styles['navbar--opened']]: opened
      })}>
      {isLoaded ? (
        <div className={styles.navbar__account} onClick={handleGoToAccount}>
          <Userimage userData={userdata} className={styles.navbar__accountImg}/>
          <span className={styles.navbar__accountUsername}>{`${userdata?.first_name || ''} ${userdata?.last_name || ''}`.trim()}</span>
        </div>
      ) : (
        <div className={styles.navbar__account} onClick={handleGoToAccount}>
          <Image className={styles.navbar__loader} src="/images/icons/loading.svg" alt="loading..."/>
        </div>
      )}

      <CafeSelector/>

      <div className={styles.navbar__container}>
        {navigation(router.asPath, !!selectedCafe).map((item, index)=>{
          return (
          <Navitem
            key={index}
            {...item}
          >
            {item.label}
          </Navitem>
          )
        })}
      </div>
      <Image className={styles.navbar__brand} src="/images/icons/cafe-brand-colored.svg" alt="" />
    </nav>
    </>
  )
}

export default Navbar;