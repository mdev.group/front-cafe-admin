import React, { useCallback, useMemo, useState } from "react";
import classNames from "classnames";

import styles from './CafeSelector.module.scss';
import { useDispatch, useSelector } from "react-redux";
import { IRootReducer } from "core/redux/reducers";
import { ICafeDTO } from "core/redux/types/dto/cafe";
import Image from "core/uikit/Image";
import Dropdown from "core/uikit/Dropdown";
import { setSelectedCafeID } from "core/redux/actions/cafe";
import { ownedCafes_selector, selectedCafe_selector } from "core/redux/selectors";
import Link from "next/link";

const Navitem: React.FC = ()=>{
  const dispatch = useDispatch();

  const ownedCafes = useSelector(ownedCafes_selector);

  const selectedCafe = useSelector(selectedCafe_selector);

  const isLoaded = useMemo(()=>{
    return [ownedCafes, selectedCafe].every(item=>item !== undefined);
  }, [ownedCafes, selectedCafe])

  const [openedChoose, setOpenedChoose] = useState(false);

  const handleOpen = useCallback((e)=>{
    if(e.target === e.currentTarget) {
      if (ownedCafes && ownedCafes.length > 1) {
        setOpenedChoose(true);
      }
    }
  }, [ownedCafes]);
  
  const handleClose = useCallback(()=>{
    setOpenedChoose(false);
  },[])

  const handleChoose = useCallback((ID: string)=>()=>{
    dispatch(setSelectedCafeID(ID))
    setOpenedChoose(false);
  }, [dispatch])

  return (
    <>
      {isLoaded ? (
        <>
          {selectedCafe ? (
            <div
              className={classNames(styles.cafeSelector, {
                [styles['cafeSelector--selector']]: ownedCafes?.length > 1
              })}
              onClick={handleOpen}
            >
              <span className={styles.cafeSelector__selectedname}>{selectedCafe.name} ({selectedCafe.code})</span>
              {openedChoose && <Dropdown
                items={ownedCafes?.map((item)=>(
                  {
                    label: item.name,
                    onClick: handleChoose(item._id)
                  }
                )) || []}
                onClose={handleClose}
                classNames={{
                  main: styles.cafeSelector__dropdown
                }}
              />}
            </div>
          ) : (
            <Link
              href="/panel/cafes"
            >
              <a className={styles.cafeSelector}>
                <span>Откройте заведение</span>
              </a>
            </Link>
          )}
        </>
      ) : (
        <div className={styles.cafeSelector}>
          <Image className={styles.cafeSelector__loader} src="/images/icons/loading.svg" alt="loading..."/>
        </div>
      )}
    </>
  );
}

export default Navitem;