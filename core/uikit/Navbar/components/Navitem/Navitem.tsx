import React, { useCallback, useEffect, useMemo, useState } from "react";
import classNames from "classnames";
import AnimateHeight from 'react-animate-height';

import styles from './Navitem.module.scss';
import { useRouter } from "next/router";

export interface IChild {
  condition?: boolean;
  label: string;
  items?: IChild[];
  url?: string;
}

interface IProps {
  condition?: boolean;
  items?: IChild[];
  url?: string;
  className?: string;
}

const Navitem: React.FC<IProps> = ({
  children,
  condition = true,
  items = [],
  url,
  className
})=>{
  const router = useRouter();

  const [expanded, setExpanded] = useState<boolean | undefined>(undefined);

  const hasChilds = useMemo(()=>{
    return items.some(item=>!!item.condition)
  }, [items])
  
  const handleExpand = useCallback(()=>{
    if(hasChilds) {
      setExpanded(!expanded);
    } else if(url) {
      router.push(url);
    }
  }, [hasChilds, expanded, url, router])

  const calculateUrlActive = useCallback((item: IProps):boolean => {
    return item.condition ? (item.url === router.asPath || item.items?.some(inner=>calculateUrlActive(inner)) || false) : false;
  }, [router.asPath])

  const urlActive = useMemo(()=>{
    const res = calculateUrlActive({
      condition,
      items,
      url
    })

    return res;
  }, [calculateUrlActive, condition, items, url])
  
  useEffect(()=>{
    if(urlActive && expanded === undefined && hasChilds) {
      setExpanded(!expanded);
    }
  }, [urlActive, hasChilds, expanded])


  if(typeof condition == 'boolean') {
    if(!condition) {
      return null;
    }
  }

  return (
    <div
      className={classNames(className, styles.navitem, {
        [styles['navitem--branch']]: hasChilds,
        [styles['navitem--active']]: expanded,
        [styles['navitem--urlActive']]: urlActive
      })}
      onClick={(e)=>{e.stopPropagation();handleExpand()}}
    >
      <span>{children}</span>
      <AnimateHeight
        duration={200}
        height={expanded? 'auto' : 0}
        className={styles.navitem__ah}
      >
        <div className={styles.navitem__inner}>
          {items.map((item, index)=>{
            return (
              <Navitem
                className={styles['navitem--child']}
                key={index}
                {...item}
              >
                {item.label}
              </Navitem>
            )
          })}
        </div>
      </AnimateHeight>
    </div>
  );
}

export default Navitem;