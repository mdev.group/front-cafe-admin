import type { IChild } from "./components/Navitem/Navitem";

export const navigation = (currUrl: string, isCafeSelected: boolean): IChild[] => [
  {
    label: 'Сводка',
    condition: true,
    url: '/panel/home'
  },
  {
    label: 'Заведения',
    condition: true,
    url: '/panel/cafes'
  },
  {
    label: 'Персонал',
    condition: isCafeSelected,
    items: [
      {
        label: 'Список',
        condition: true,
        url: '/panel/employes',
      },
      {
        label: 'Смены',
        condition: true,
        url: '/panel/workshifts'
      },
      {
        label: 'Карточка сотрудника',
        condition: currUrl.startsWith('/panel/employes/'),
        url: currUrl
      }
    ]
  },
  {
    label: 'Меню',
    condition: isCafeSelected,
    url: '/panel/items'
  },
]