import classNames from 'classnames';
import { CSSProperties } from 'react';
import styles from './PageCard.module.scss';

interface IProps {
  className?: string;
  flexGrow?: number;
}

const PageCard: React.FC<IProps> = ({
  children,
  className,
  flexGrow
}) => {
  return (
    <div style={{flexGrow}} className={classNames(className, styles.pageCard)}>
      {children}
    </div>
  )
}

export default PageCard;