import { useCallback } from 'react';
import TelegramLoginButton from 'react-telegram-login';
import Button from '../Button';
import { mockData } from './constants';

import styles from './TgAuth.module.scss';

const isDev = process.env.NODE_ENV != "production";

interface IProps {
  onAuth: (data: any) => void;
}

const TgAuth: React.FC<IProps> = ({
  onAuth
}) => {
  const handleAuth = useCallback(() => {
    if(onAuth) {
      onAuth(mockData);
  }
  }, [onAuth]);

  return (
    <div className={styles.wrapper}>
      {isDev ? (
        <Button onClick={handleAuth} className={styles.devButton}>
          Войти как Антон
        </Button>
      ) : (
        <TelegramLoginButton dataOnauth={onAuth} botName="mdevgroup_bot" />
      )}
    </div>
  )
}

export default TgAuth;