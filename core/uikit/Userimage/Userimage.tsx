
import classNames from "classnames";
import type { IUserData } from "core/redux/reducers/user";
import Image from "core/uikit/Image";

import styles from "./Userimage.module.scss";

interface IProps {
  userData?: IUserData;
  className?: string;
}

const Userimage: React.FC<IProps> = ({
  userData,
  className
}) => { 
  if(userData?.userimage) {
    return <Image className={classNames(className, styles.accountImg)} src={userData.userimage} alt="" />
  } else {
    return <div className={classNames(className, styles.accountImg, styles['accountImg--chars'])}>
      {userData?.first_name.toUpperCase().substring(0, 2)}
    </div>
  }
}

export default Userimage;