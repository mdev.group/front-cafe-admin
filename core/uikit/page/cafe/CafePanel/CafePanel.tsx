import classNames from "classnames";
import { badges, categories } from "core/constants/categories";
import { useForm } from "core/hooks/useForm";
import { beginCafeUpdate } from "core/redux/actions/cafe";
import { ICafeDTO } from "core/redux/types/dto/cafe";
import AddressInput from "core/uikit/AddressInput";
import Button from "core/uikit/Button";
import FoodCategory from "core/uikit/FoodCategory";
import ImageUploader from "core/uikit/ImageUploader";
import Input from "core/uikit/Input";
import { PageCard, PageRow } from "core/uikit/PanelContainer";
import Toggle from "core/uikit/Toggle";
import { useCallback, useEffect } from "react";
import { useDispatch } from "react-redux";

import styles from './CafePanel.module.scss'

interface IProps {
  data: ICafeDTO;
}

const CafePanel: React.FC<IProps> = ({
  data
}) => {
  const dispatch = useDispatch();

  const handleUpdateCafe = (formData: any) => {
    dispatch(beginCafeUpdate({
      id: data._id,
      newData: {
        ...formData,
        loyaltyType: formData.loyaltyType ? 2 : 0
      }
    }))

    reinit()
  };

  var {
    onChangeField,
    handleSubmit,
    formData,
    reinit,
    isChanged
  } = useForm(handleUpdateCafe, {
    name: data.name,
    address: data.address,
    closed: data.closed,
    photo_url: data.photo_url,
    loyaltyType: !!data.loyaltyType,
    categories: data.categories || [],
    badges: data.badges || [],
    acceptOnlineOrders: data.acceptOnlineOrders || false,
    location: data.location
  });

  useEffect(() => {
    reinit()
  }, [data])

  const handleSelectCategory = useCallback((category) => () => {
    if(formData.categories.includes(category)) {
      onChangeField('categories')(formData.categories.filter((cat: string) => cat !== category) as any)
    } else {
      onChangeField('categories')([...formData.categories, category].sort() as any)
    }
  }, [onChangeField, formData])

  const handleBadgeToggle = useCallback((key) => () => {
    if(formData.badges.includes(key)) {
      onChangeField('badges')(formData.badges.filter((cat: string) => cat !== key) as any)
    } else {
      onChangeField('badges')([...formData.badges, key].sort() as any)
    }
  }, [onChangeField, formData])

  return (
    <PageRow className={styles.panels__wrapper}>
      <PageCard className={styles.panel}>
        <div>
          <ImageUploader
            className={styles.panel__image}
            imageUrl={formData.photo_url}
            placeholderUrl={'/images/icons/blank/cafe.svg'}
            onUpload={onChangeField('photo_url')}
          />
          <div></div>
        </div>
        <div>
          <Input
            label="Название"
            value={formData['name']}
            onChange={onChangeField('name')}
            name="name"
          />
          <Input
            label="Адрес"
            value={formData['address']}
            onChange={onChangeField('address')}
            name="address"
          />
          <AddressInput
            value={formData.location}
            onChange={onChangeField('location') as any}
          />
          

          <div className={styles.panel__row}>
            <div className={styles.panel__swithWrapper}>
              Система лояльности
              <Toggle
                value={!!formData['loyaltyType']}
                onChange={onChangeField('loyaltyType')}
              />
            </div>
            <div className={styles.panel__swithWrapper}>
              Заведение - {formData['closed'] ? 'Закрыто' : 'Открыто'}
              <Toggle
                invert
                value={formData['closed']}
                onChange={onChangeField('closed')}
              />
            </div>
          </div>

          <Button
            onClick={handleSubmit}
            size="md"
            theme="primary"
            className={styles.panel__submit}
            disabled={!isChanged}
          >
            Сохранить
          </Button>
        </div>
      </PageCard>

      <PageCard className={classNames(styles.card, styles.card__categories)}>
        <h4>Категории</h4>
        <div className={styles.card__categoriesList}>
          {categories.map((cat, index) => (
            <FoodCategory
              key={index}
              label={cat.label}
              icon={cat.icon}
              color={cat.color}
              active={formData.categories.includes(cat.label)}
              onClick={handleSelectCategory(cat.label)}
            />
          ))}
        </div>

        <h4>Настройка бейджей</h4>
        <div className={styles.card__categoriesList}>
          {badges.map((bdg, index) => (
            <Toggle
              key={index}
              label={bdg.label}
              value={formData.badges.includes(bdg.key)}
              onChange={handleBadgeToggle(bdg.key)}
            />
          ))}
          <Toggle
            label="Онлайн заказ"
            value={formData.acceptOnlineOrders}
            onChange={() => onChangeField('acceptOnlineOrders')(!formData.acceptOnlineOrders as any)}
          />
        </div>

        <Button
          size="md"
          theme="primary"
          className={styles.panel__submit}
          onClick={handleSubmit}
          disabled={!isChanged}
        >
          Сохранить
        </Button>
      </PageCard>
    </PageRow>
  )
}

export default CafePanel;