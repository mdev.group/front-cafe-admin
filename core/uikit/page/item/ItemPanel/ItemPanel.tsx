import { useForm } from "core/hooks/useForm";
import { beginItemUpdate } from "core/redux/actions/item";
import { globalCompositions_selector, selectedCafe_selector } from "core/redux/selectors";
import { IComposition, ICompositionAssign, IItemDTO } from "core/redux/types/dto/item"
import Button from "core/uikit/Button";
import Image from "core/uikit/Image"
import ImageUploader from "core/uikit/ImageUploader";
import Input from "core/uikit/Input";
import ModalContainer, { ModalForm } from "core/uikit/ModalContainer";
import { PageCard, PageRow } from "core/uikit/PanelContainer";
import Select from "core/uikit/Select";
import {getBase64} from "core/utils/getBase64";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Ingredient from "./components/Ingredient/Ingredient";
import NewCompositionModal from "./components/newCompositionModal/NewCompositionModal";

import styles from './ItemPanel.module.scss';

interface IProps {
  data: IItemDTO;
}

const ItemCard: React.FC<IProps> = ({
  data
}) => {
  const dispatch = useDispatch();

  const [createModalOpen, setCreateModalOpen] = useState<boolean>(false);

  const handleUpdateCafe = useCallback((formData) => {
    formData.composition = formData.composition.map((item: ICompositionAssign) => ({
      ...item,
      price: item.isEditable ? item.price || '0' : item.price
    }))
    dispatch(beginItemUpdate({
      id: data._id,
      newData: formData
    }))
  }, [dispatch, data._id])

  const {
    onChangeField,
    handleSubmit,
    formData,
    reinit,
    isChanged
  } = useForm(handleUpdateCafe, {
    ...data
  });

  const {
    onChangeField: compChangeField,
    handleSubmit: compHandleSubmit,
    formData: compFormData,
    isChanged: compIsChanged,
    reinit: compReinit
  } = useForm(handleUpdateCafe, {
    ...data,
    composition: data.composition.sort((a, b) => +b.isEditable - +a.isEditable)
  });

  useEffect(() => {
    reinit();
    compReinit();
  }, [data])

  return (
    <>
      {createModalOpen && (
        <NewCompositionModal itemID={data._id} onClose={() => setCreateModalOpen(false)} />
      )}

      <PageRow className={styles.card__wrapper}>
        <PageCard className={styles.card}>
          <div className={styles.card__imageWrapper}>
            <ImageUploader
              className={styles.card__image}
              imageUrl={formData.photo_url}
              onUpload={onChangeField('photo_url')}
              placeholderUrl="/images/icons/blank/food.svg"
            />
          </div>

          <div className={styles.card__form}>
            <Input
              className={styles.card__input}
              label="Название"
              name="name"
              value={formData['name']}
              onChange={onChangeField('name')}
            />
            <Input
              className={styles.card__input}
              label="Масса"
              name="max"
              value={formData['mass']}
              onChange={onChangeField('mass')}
            />
            <Input
              className={styles.card__input}
              label="Стоимость"
              name="price"
              value={formData['price']}
              onChange={onChangeField('price')}
            />
            <Input
              className={styles.card__input}
              label="Категория"
              name="category"
              value={formData['category']}
              onChange={onChangeField('category')}
            />

            <div className={styles.card__actions}>
              <Button theme="primary" size='md' onClick={handleSubmit} disabled={!isChanged}>
                Сохранить
              </Button>
            </div>
          </div>
        </PageCard>

        <PageCard className={styles.card}>
          <div className={styles.card__compositionWrapper}>
            <h4>Состав</h4>
            <div className={styles.card__compositionList}>
              {compFormData.composition.map((assign: ICompositionAssign, index: number) => {
                const ingData = data.compositionItems.find((item) => item._id === assign.id);
                if(ingData) {
                  return (
                    <Ingredient
                      key={assign.id}
                      ingData={data.compositionItems.find((item) => item._id === assign.id)!}
                      assignData={assign}
                      itemID={data._id}
                      onChange={(key: string) => { return compChangeField(`composition.${index}.${key}`) }}
                    />
                  )
                }
              })}
            </div>
            <div className={styles.card__compositionNewRow}>
              <Button
                theme='primary'
                size='md'
                onClick={compHandleSubmit}
                disabled={!compIsChanged}
              >
                Сохранить
              </Button>

              <Button theme='primary' size='md' onClick={() => setCreateModalOpen(true)}>
                Добавить ингредиент
              </Button>
            </div>
          </div>
        </PageCard>
      </PageRow>
    </>
  )
}

export default ItemCard;