import { beginItemUpdate } from 'core/redux/actions/item';
import { getItemFromSelectedCafe_selector } from 'core/redux/selectors';
import { IComposition, ICompositionAssign } from 'core/redux/types/dto/item';
import Button from 'core/uikit/Button';
import Image from 'core/uikit/Image';
import Input from 'core/uikit/Input';
import Toggle from 'core/uikit/Toggle';
import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styles from './Ingredient.module.scss';

interface IProps {
  ingData: IComposition;
  assignData: ICompositionAssign;
  itemID: string;
  onChange: (key: string) => (value: any) => void
}

const Ingredient: React.FC<IProps> = ({
  ingData,
  assignData,
  itemID,
  onChange
}) => {
  const dispatch = useDispatch();
  const itemData = useSelector(getItemFromSelectedCafe_selector(itemID))

  const handleRemove = useCallback((ID) => () => {
    if(itemData) {
      dispatch(beginItemUpdate({
        id: itemData._id,
        newData: {
          composition: itemData.composition.filter((item) => item.id !== ID)
        }
      }))
    }
  }, [dispatch, itemData])

  return (
    <div className={styles.card}>
      <span className={styles.card__name}>{ingData.name}</span>
      <div className={styles.card__spacer}></div>
      <div className={styles.card__inputs}>
        <Input
          value={assignData.mass}
          label="Грамм"
          onChange={onChange('mass')}
          className={styles.card__input}
        />
        <Input
          value={assignData.price}
          label="Цена"
          onChange={onChange('price')}
          className={styles.card__input}
        />
        <Toggle
          value={assignData.isEditable}
          onChange={onChange('isEditable')}
          label='Опция'
        />
      </div>
      {ingData.pfc && (
        <div className={styles.card__pfcWrapper} title={`На ${assignData.mass} грамм`}>
          <div className={styles.card__pfc}>
            <span className={styles.card__pfcTitle}>Белки</span>
            <span>{(ingData.pfc.proteins / 100 * assignData.mass).toFixed(1)}</span>
          </div>
          <div className={styles.card__pfc}>
            <span className={styles.card__pfcTitle}>Жиры</span>
            <span>{(ingData.pfc.fats / 100 * assignData.mass).toFixed(1)}</span>
          </div>
          <div className={styles.card__pfc}>
            <span className={styles.card__pfcTitle}>Углеводы</span>
            <span>{(ingData.pfc.carbohydrates / 100 * assignData.mass).toFixed(1)}</span>
          </div>
        </div>
      )}
      <div className={styles.card__actions}>
        <Button onClick={handleRemove(assignData.id)}>
          <Image
            src='/images/icons/trash.svg'
            alt=''
            className={styles.card__icon}
          />
        </Button>
      </div>
    </div>
  )
}

export default Ingredient;