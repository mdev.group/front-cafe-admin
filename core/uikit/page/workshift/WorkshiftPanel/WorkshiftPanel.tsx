import classNames from 'classnames';
import { beginWorkshiftAssign, beginWorkshiftDelelte } from 'core/redux/actions/workshift';
import { selectedCafe_selector } from 'core/redux/selectors';
import { IWorkShiftDTO } from 'core/redux/types/dto/workshift';
import Button from 'core/uikit/Button';
import Grid from 'core/uikit/Grid';
import { calculateEmployeeTime } from 'core/utils/calculateEmployeeTime';
import { calculateTimestamp } from 'core/utils/calculateTime';
import { mapDayOfWeek } from 'core/utils/mapDayOfWeek';
import { numToLength } from 'core/utils/numToLength';
import { useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import EmployeeCard from '../../employee/EmployeeCard';
import styles from './WorkshiftPanel.module.scss';

interface IProps {
  data: IWorkShiftDTO | null;
}

const WorkshiftListPanel: React.FC<IProps> = ({
  data
}) => {
  const dispatch = useDispatch();
  const selectedCafe = useSelector(selectedCafe_selector);

  const begin = useMemo(() => data && (`${numToLength(Math.floor(data.beginTime / 60), 2)}:${numToLength(Math.floor(data.beginTime % 60), 2)}`), [data])
  const end = useMemo(() => data && (`${numToLength(Math.floor(data.endTime / 60), 2)}:${numToLength(Math.floor(data.endTime % 60), 2)}`), [data])

  const employees = useMemo(()=> {
    return selectedCafe?.employees.filter((item) => {
      return data?.employeeIDs.includes(item._id);
    }) || []
  }, [data, selectedCafe])

  const handleDeselectEmployee = (ID: string) => () => {
    if(data) {
      dispatch(beginWorkshiftAssign({
        id: data._id,
        employeeID: ID,
        value: false
      }))
    }
  };

  const handleDeleteWorkShift = () => {
    if(data) {
      dispatch(beginWorkshiftDelelte(data._id))
    }
  };

  return (
    <div className={styles.panel}>
      {selectedCafe && data ? (
        <>
          <div className={styles.panel__headline}>
            <h3>{`Смена ${begin}-${end} (${mapDayOfWeek(data.dayOfWeek)})`}</h3>
            <Button disabled={employees.length > 0} theme={'danger'} size='sm' round variant='outline' onClick={handleDeleteWorkShift}>
                Удалить
            </Button>
          </div>
          <Grid
            itemSize="calc(50% - 8px)"
            gap="8px"
          >
            {employees.map((employee)=>(
              <EmployeeCard
                key={employee._id}
                employeeInfo={employee}
                onClick={handleDeselectEmployee(employee._id)}
                appendBadge={calculateTimestamp(calculateEmployeeTime(employee._id, selectedCafe))}
              />
            ))}
            </Grid>
        </>
      ) : (
        <div className={styles.panel__placeholder}>
          <span className={styles.panel__placeholderTitle}>
            Выберите смену
          </span>
          <p className={styles.panel__placeholderText}>
            для получения краткой{'\n'}информации по ней
          </p>
        </div>
      )}
    </div>
  )
}

export default WorkshiftListPanel;