import "../styles/globals.css";
import type { AppProps } from "next/app";
import Head from "next/head";
import { initStore, getStore } from "../core/redux/index";
import { Provider } from "react-redux";
import { useEffect, useRef, useState } from "react";
import authTokenService from "core/services/authTokenService";
import { TStore } from "core/redux/storeDataProvider";
import Navbar from "core/uikit/Navbar";
import { useRouter } from "next/router";

function MyApp({ Component, pageProps }: AppProps) {
  const [store, setStore] = useState<TStore>();

  useEffect(() => {
    initStore();
    const store = getStore();
    authTokenService.init(store.dispatch);
    setStore(store);
  }, [])

  const router = useRouter();

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no, user-scalable=no, viewport-fit=cover"
        />
      </Head>
      {store && (
        <Provider store={store}>
          {router.asPath.indexOf('/panel') !== -1 && <Navbar />}
          <Component {...pageProps} />
        </Provider>
      )}
    </>
  );
}

export default MyApp;
