import { seoData as seoDataMock } from "core/constants/seoData";
import useDebouncedMemo from "core/hooks/useDebouncedMemo";
import { useForm } from "core/hooks/useForm";
import { IRootReducer } from "core/redux/reducers";
import { ICafeDTO } from "core/redux/types/dto/cafe";
import AlertBanner from "core/uikit/AlertBanner";
import Button from "core/uikit/Button";
import CafeCard from "core/uikit/CafeCard";
import CreateButton from "core/uikit/CreateButton";
import Grid from "core/uikit/Grid";
import Input from "core/uikit/Input";
import CafePanel from "core/uikit/page/cafe/CafePanel";
import PanelContainer from "core/uikit/PanelContainer";
import type { NextPage } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import { useCallback, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const Home: NextPage = () => {
  const router = useRouter();
  const { id: cafeID } = router.query;
  

  const ownedCafes = useSelector<IRootReducer, ICafeDTO[] | undefined>((store) => store.user.ownedCafes);
  const cafe = useMemo(() => ownedCafes?.find((cafe) => cafe._id === cafeID), [cafeID, ownedCafes]);

  const isLoaded = useDebouncedMemo(() => {
    return [ownedCafes, cafe].every(item => item !== undefined);
  }, [ownedCafes], 200)

  return (
    <div>
      <PanelContainer loading={!isLoaded}>
        <h2>Карточка заведения</h2>
        {cafe && <CafePanel data={cafe}/>}
      </PanelContainer>
    </div>
  );
};

export default Home;
