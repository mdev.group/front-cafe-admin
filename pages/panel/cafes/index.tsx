import { seoData as seoDataMock } from "core/constants/seoData";
import useDebouncedMemo from "core/hooks/useDebouncedMemo";
import { useForm } from "core/hooks/useForm";
import { beginCafeCreate } from "core/redux/actions/cafe";
import { IRootReducer } from "core/redux/reducers";
import { ICafeDTO } from "core/redux/types/dto/cafe";
import AlertBanner from "core/uikit/AlertBanner";
import Button from "core/uikit/Button";
import CafeCard from "core/uikit/CafeCard";
import CreateButton from "core/uikit/CreateButton";
import Grid from "core/uikit/Grid";
import Input from "core/uikit/Input";
import ModalContainer, { ModalForm } from "core/uikit/ModalContainer";
import PanelContainer from "core/uikit/PanelContainer";
import type { NextPage } from "next";
import Head from "next/head";
import Link from "next/link";
import { useCallback, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const Home: NextPage = () => {
  const dispatch = useDispatch();

  const seoData = seoDataMock['/panel/home'];
  const ownedCafes = useSelector<IRootReducer, ICafeDTO[] | undefined>((store) => store.user.ownedCafes);
  const [createModalShow, setCreateModalShow] = useState(false);

  const isLoaded = useDebouncedMemo(() => {
    return [ownedCafes].every(item => item !== undefined);
  }, [ownedCafes], 200)

  const handleOpenCreateCafe = useCallback(()=>{
    setCreateModalShow(true);
  }, []);

  const handleCloseCreateCafe = useCallback(()=>{
    setCreateModalShow(false);
  }, []);

  const handleCreateCafe = ()=>{
    dispatch(beginCafeCreate({
      name: formData['name'],
      address: formData['address']?.value || formData['address']
    }));

    handleCloseCreateCafe();
  };

  var {
    onChangeField,
    handleSubmit,
    formData
  } = useForm(handleCreateCafe);

  const isSubmittable = useMemo(() => {
    const {name, address} = formData;
    return name && address;
  }, [formData])

  return (
    <div>
      <Head>
        <title>{seoData.title}</title>
      </Head>

      {createModalShow && (
        <ModalContainer
          onClose={handleCloseCreateCafe}
          title="Создание заведения"
        >
          <ModalForm>
            <Input
              label="Название"
              name="name"
              value={formData['name']}
              onChange={onChangeField('name')}
            />
            <Input
              type="address"
              label="Адрес"
              name="Address"
              value={formData['address']}
              onChange={onChangeField('address')}
            />

            <Button
              size="md"
              theme="primary"
              onClick={handleSubmit}
              disabled={!isSubmittable}
            >
              Создать
            </Button>
          </ModalForm>
        </ModalContainer>
      )}

      <PanelContainer loading={!isLoaded}>
        <h2>Ваши заведения</h2>
        {ownedCafes?.length === 0 ? (
          <AlertBanner
            title="Нет зарегестрированных заведений"
            buttonText="Создать"
            onClick={handleOpenCreateCafe}
          />
        ) : (
          <Grid
            itemSize="calc((100% / 3) - 16px)"
          >
            {ownedCafes?.map((cafe)=>(
              <Link
                key={cafe._id}
                href={`/panel/cafes/${cafe._id}`}
                passHref={true}
              >
                <a>
                  <CafeCard
                    cafeInfo={cafe}
                  />
                </a>
              </Link>
            ))}
            <CreateButton onClick={handleOpenCreateCafe}>
              Добавить заведение
            </CreateButton>
          </Grid>
        )}
      </PanelContainer>
    </div>
  );
};

export default Home;
