import { useCallback, useMemo, useState } from "react";
import useDebouncedMemo from "core/hooks/useDebouncedMemo";
import { IRootReducer } from "core/redux/reducers";
import { ICafeDTO } from "core/redux/types/dto/cafe";
import PanelContainer from "core/uikit/PanelContainer";
import type { NextPage } from "next";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import EmployeeCard from "core/uikit/page/employee/EmployeeCard";

const Home: NextPage = () => {
  const dispatch = useDispatch();
  const router = useRouter()
  const { employeeID } = router.query

  const ownedCafes = useSelector<IRootReducer, ICafeDTO[] | undefined>((store)=>store.user.ownedCafes);
  const selectedCafeID = useSelector<IRootReducer, string | null | undefined>((store) => store.user.selectedCafeID);
  const selectedCafe = useMemo(() => ownedCafes?.find((item) => item._id === selectedCafeID), [selectedCafeID, ownedCafes]);

  const employee = useMemo(()=>{
    return selectedCafe?.employees.find((item)=>item._id === employeeID)
  }, [selectedCafe, employeeID])

  const isLoaded = useDebouncedMemo(() => {
    return [selectedCafe].every(item => item !== undefined);
  }, [selectedCafe], 200)

  return (
    <div>
      <PanelContainer loading={!isLoaded}>
        <h2>Личная карточка</h2>
        <div>
          
        </div>
        <div>
          {employee && <EmployeeCard
            extended
            employeeInfo={employee!}
          />}
          <div></div>
        </div>
      </PanelContainer>
    </div>
  );
};

export default Home;
