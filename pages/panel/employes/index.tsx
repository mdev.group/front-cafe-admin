import { seoData as seoDataMock } from "core/constants/seoData";
import useDebouncedMemo from "core/hooks/useDebouncedMemo";
import { useForm } from "core/hooks/useForm";
import { beginCafeCreate, beginEmployeeCreate } from "core/redux/actions/cafe";
import { IRootReducer } from "core/redux/reducers";
import { ICafeDTO } from "core/redux/types/dto/cafe";
import AlertBanner from "core/uikit/AlertBanner";
import Button from "core/uikit/Button";
import CreateButton from "core/uikit/CreateButton";
import EmployeeCard from "core/uikit/page/employee/EmployeeCard";
import Grid from "core/uikit/Grid";
import Input from "core/uikit/Input";
import ModalContainer, { ModalForm } from "core/uikit/ModalContainer";
import PanelContainer from "core/uikit/PanelContainer";
import redirectTo from "core/utils/redirectTo";
import type { NextPage } from "next";
import Head from "next/head";
import { useCallback, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { calculateTimestamp } from "core/utils/calculateTime";
import { calculateEmployeeTime } from "core/utils/calculateEmployeeTime";

const Home: NextPage = () => {
  const dispatch = useDispatch();

  
  const ownedCafes = useSelector<IRootReducer, ICafeDTO[] | undefined>((store)=>store.user.ownedCafes);
  const selectedCafeID = useSelector<IRootReducer, string | null | undefined>((store) => store.user.selectedCafeID);
  const selectedCafe = useMemo(() => ownedCafes?.find((item) => item._id === selectedCafeID), [selectedCafeID, ownedCafes]);

  const [createModalShow, setCreateModalShow] = useState(false);

  const isLoaded = useDebouncedMemo(() => {
    return [selectedCafe].every(item => item !== undefined);
  }, [selectedCafe], 200)

  const handleOpenCreateEmployee = useCallback(()=>{
    setCreateModalShow(true);
  }, []);

  const handleCloseCreateEmployee = useCallback(()=>{
    setCreateModalShow(false);
  }, []);

  const handleCreateEmployee = ()=>{
    const {
      first_name,
      last_name,
      position
    } = formData;
    
    dispatch(beginEmployeeCreate({
      first_name,
      last_name,
      position
    }))

    handleCloseCreateEmployee()
  };

  const handleOpenEmployee = (ID: string) => () => {
    redirectTo(`/panel/employes/${ID}`)
  };

  var {
    onChangeField,
    handleSubmit,
    formData
  } = useForm(handleCreateEmployee);

  const isSubmittable = useMemo(() => {
    const {
      first_name,
      last_name,
      position
    } = formData;
    return first_name && last_name && position;
  }, [formData])

  return (
    <div>
      {createModalShow && (
        <ModalContainer
          onClose={handleCloseCreateEmployee}
          title="Добавление сотрудника"
        >
          <ModalForm>
            <Input
              label="Имя"
              name="first_name"
              value={formData['first_name']}
              onChange={onChangeField('first_name')}
            />
            <Input
              label="Фамилия"
              name="last_name"
              value={formData['last_name']}
              onChange={onChangeField('last_name')}
            />
            <Input
              label="Должность"
              name="position"
              value={formData['position']}
              onChange={onChangeField('position')}
            />
            <Button
              size="md"
              theme="primary"
              onClick={handleSubmit}
              disabled={!isSubmittable}
            >
              Добавить
            </Button>
          </ModalForm>
        </ModalContainer>
      )}

      <PanelContainer loading={!isLoaded}>
        <h2>Персонал заведения</h2>
        {selectedCafe?.employees?.length === 0 ? (
          <AlertBanner
            title="Персонал ещё не добавлен"
            buttonText="Добавить"
            onClick={handleOpenCreateEmployee}
          />
        ) : (
          <Grid
            itemSize="calc(50% - 8px)"
            gap="8px"
          >
            {selectedCafe?.employees?.map((employee)=>(
              <EmployeeCard
                key={employee._id}
                employeeInfo={employee}
                onClick={handleOpenEmployee(employee._id)}
                appendBadge={calculateTimestamp(calculateEmployeeTime(employee._id, selectedCafe))}
              />
            ))}
            <CreateButton onClick={handleOpenCreateEmployee}>
              Добавить сотрудника
            </CreateButton>
          </Grid>
        )}
      </PanelContainer>
    </div>
  );
};

export default Home;
