import { seoData as seoDataMock } from "core/constants/seoData";
import useDebouncedMemo from "core/hooks/useDebouncedMemo";
import { getCafeOrders } from "core/redux/actions/cafe";
import { IRootReducer } from "core/redux/reducers";
import { cafeOrders_selector, selectedCafe_selector } from "core/redux/selectors";
import { EOrderStatus } from "core/redux/types/dto/order";
import AlertBanner from "core/uikit/AlertBanner";
import Button from "core/uikit/Button";
import PanelContainer, { PageCard, PageRow } from "core/uikit/PanelContainer";
import { numToLength } from "core/utils/numToLength";
import redirectTo from "core/utils/redirectTo";
import moment from "moment";
import type { NextPage } from "next";
import Head from "next/head";
import { useCallback, useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";

import styles from 'styles/pages/MainPage.module.scss';

const Home: NextPage = () => {
  const dispatch = useDispatch();
  const seoData = seoDataMock['/panel/home'];
  const selectedCafe = useSelector(selectedCafe_selector);
  const cafeOrders = useSelector(cafeOrders_selector);
  
  const isLoaded = useDebouncedMemo(()=>{
    return [selectedCafe, cafeOrders].every(item=>item !== undefined);
  }, [selectedCafe], 200)

  const handleOpenCafesList = useCallback(() => {
    redirectTo('/panel/cafes')
  }, []);

  useEffect(() => {
    if(selectedCafe) {
      dispatch(getCafeOrders(selectedCafe._id))
    }
  }, [dispatch, selectedCafe])

  const currentWorkshift = useMemo(() => {
    return selectedCafe?.workshifts.find((shift) => {
      const now = moment();
      const day = now.weekday();
      return shift.dayOfWeek == day
    })
  }, [selectedCafe])

  const lastDayWorkshift = useMemo(() => {
    if(currentWorkshift) {
      const day = currentWorkshift?.dayOfWeek;
      return selectedCafe?.workshifts.reduce((prev, shift) => {
        if(shift.dayOfWeek == day && shift.endTime > prev.endTime) {
          return shift;
        }
        return prev;
      }, currentWorkshift);
    }
  }, [currentWorkshift, selectedCafe])

  const activeOrders = useMemo(() => {
    return cafeOrders.filter((ord) => ord.status !== EOrderStatus.done).length;
  }, [cafeOrders])

  const timePerOrder = useMemo(() => {
    const ordList = cafeOrders.filter((ord) => ord.status === EOrderStatus.done);
    let avgTime = ordList.reduce((prev, curr) => {
      if(prev === -1) {
        return curr.endTime - curr.pickedTime;
      }

      if(curr.endTime && curr.pickedTime) {
        return prev + (curr.endTime - curr.pickedTime);
      }

      return prev;
    }, -1) / ordList.length / 1000;

    avgTime = Math.max(0, avgTime);

    return [
      numToLength(Math.floor(avgTime / 60), 2),
      numToLength(Math.round(avgTime % 60), 2)
    ].join(':');
  }, [cafeOrders])

  const todayDoneOrders = useMemo(() => {
    return cafeOrders.filter((ord) => (
      ord.status === EOrderStatus.done && moment(ord.beginTime).isAfter(moment().subtract(24, 'hour'))
    ));
  }, [cafeOrders])

  const avgPrice = useMemo(() => {
    const ordList = cafeOrders.filter((ord) => ord.status === EOrderStatus.done);
    let avg = ordList.reduce((prev, curr) => {
      if(prev === -1) {
        return curr.payByCard || 0 + curr.payByPaper || 0 + curr.payByScores || 0;
      }

      return (prev + (curr.payByCard || 0 + curr.payByPaper || 0 + curr.payByScores || 0));
    }, -1) / ordList.length;

    avg = Math.max(0, avg);
    return avg;
  }, [cafeOrders])
  
  const totalDayAmount = useMemo(() => {
    return todayDoneOrders.reduce((prev, curr) => {
      return prev + (curr.payByCard || 0 + curr.payByPaper || 0 + curr.payByScores || 0);
    }, 0);
  }, [todayDoneOrders])

  const updateData = useCallback(() => {
    if(selectedCafe) {
      dispatch(getCafeOrders(selectedCafe._id))
    }
  }, [dispatch, selectedCafe])
  
  return (
    <div>
      <Head>
        <title>{seoData.title}</title>
      </Head>

      <PanelContainer loading={!isLoaded}>
        {isLoaded && (
          <>
            <h2>Сводка</h2>
            {!selectedCafe && (
              <AlertBanner
                title="Откройте хотя бы одно заведение"
                buttonText="Открыть сейчас"
                onClick={handleOpenCafesList}
              />
            )}

            <PageRow>
              <PageCard className={styles.infoCard}>
                <span>{activeOrders}</span>
                <span>Активные заказы</span>
              </PageCard>
              <PageCard className={styles.infoCard}>
                <span>{timePerOrder}</span>
                <span>Среднее время на заказ</span>
              </PageCard>
              <PageCard className={styles.infoCard}>
                <span>{todayDoneOrders.length}</span>
                <span>Заказов за день</span>
              </PageCard>
            </PageRow>
            <PageRow>
              <PageCard className={styles.infoCard}>
                <span>{avgPrice.toFixed(0)} ₽</span>
                <span>Средний чек</span>
              </PageCard>
              <PageCard className={styles.infoCard}>
                <span>{totalDayAmount.toFixed(0)} ₽</span>
                <span>Выручка за день</span>
              </PageCard>
            </PageRow>
            
            {currentWorkshift && (
              <PageRow>
                <PageCard className={styles.infoCard}>
                  <span>{currentWorkshift?.employeeIDs.length || '0'}</span>
                  <span>Сотрудников в смене</span>
                </PageCard>
                <PageCard className={styles.infoCard}>
                  <span>{[numToLength(Math.floor(currentWorkshift?.endTime / 60), 2), numToLength(currentWorkshift?.endTime % 60, 2)].join(':')}</span>
                  <span>Конец смены</span>
                </PageCard>
                {lastDayWorkshift && (
                  <PageCard className={styles.infoCard}>
                    <span>{[numToLength(Math.floor(lastDayWorkshift.endTime / 60), 2), numToLength(lastDayWorkshift.endTime % 60, 2)].join(':')}</span>
                    <span>Закрытие заведения</span>
                  </PageCard>
                )}
              </PageRow>
            )}

            <div className={styles.actions}>
              <Button
                variant='outline'
                theme='primary'
                size="md"
                onClick={updateData}
              >
                Обновить информацию
              </Button>
            </div>
          </>
        )}
      </PanelContainer>
    </div>
  );
};

export default Home;
