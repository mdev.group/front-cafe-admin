import useDebouncedMemo from "core/hooks/useDebouncedMemo";
import { getItemFromSelectedCafe_selector, selectedCafe_selector } from "core/redux/selectors";
import PanelContainer from "core/uikit/PanelContainer";
import ItemPanel from "core/uikit/page/item/ItemPanel";
import type { NextPage } from "next";
import Head from "next/head";
import { useSelector } from "react-redux";
import CreateButton from "core/uikit/CreateButton";
import Grid from "core/uikit/Grid";
import AlertBanner from "core/uikit/AlertBanner";
import { useRouter } from "next/router";
import { useEffect, useMemo } from "react";

const Home: NextPage = () => {
  const router = useRouter()
  const { itemID } = router.query

  const selectedCafe = useSelector(selectedCafe_selector);
  const selectedItem = useSelector(getItemFromSelectedCafe_selector(itemID as string))

  const isLoaded = useDebouncedMemo(() => {
    return [selectedCafe].every(item => item !== undefined);
  }, [selectedCafe], 200)

  return (
    <div>
      <PanelContainer loading={!isLoaded}>
        <h2>Позиции меню</h2>

        {selectedItem && (
          <ItemPanel
            data={selectedItem}
          />
        )}
      </PanelContainer>
    </div>
  );
};

export default Home;
