import useDebouncedMemo from "core/hooks/useDebouncedMemo";
import { selectedCafe_selector } from "core/redux/selectors";
import PanelContainer from "core/uikit/PanelContainer";
import ItemCard from "core/uikit/page/item/ItemCard/index";
import type { NextPage } from "next";
import Link from "next/link";
import Head from "next/head";
import { useDispatch, useSelector } from "react-redux";
import CreateButton from "core/uikit/CreateButton";
import Grid from "core/uikit/Grid";
import AlertBanner from "core/uikit/AlertBanner";
import { useCallback, useEffect, useMemo, useState } from "react";
import redirectTo from "core/utils/redirectTo";
import { beginItemCreate } from "core/redux/actions/item";
import { useForm } from "core/hooks/useForm";
import ModalContainer, { ModalForm } from "core/uikit/ModalContainer";
import Input from "core/uikit/Input";
import Button from "core/uikit/Button";
import ImageUploader from "core/uikit/ImageUploader";

const Home: NextPage = () => {
  const dispatch = useDispatch();

  const [createModalOpen, setCreateModalOpen] = useState<boolean>(false);

  const selectedCafe = useSelector(selectedCafe_selector);

  const isLoaded = useDebouncedMemo(() => {
    return [selectedCafe].every(item => item !== undefined);
  }, [selectedCafe], 200)

  const handleCreateCafe = useCallback((formData) => {
    if (selectedCafe) {
      dispatch(beginItemCreate({
        cafeID: selectedCafe._id,
        ...formData
      }))
    }
  }, [dispatch, selectedCafe])

  const {
    onChangeField,
    handleSubmit,
    formData,
    reinit
  } = useForm(handleCreateCafe);

  useEffect(() => {
    reinit()
  }, [selectedCafe])

  const isSubmittable = useMemo(() => {
    const {
      name,
      mass,
      price
    } = formData;
    return name && mass && price;
  }, [formData])

  console.log(selectedCafe?.items)

  return (
    <>
      {createModalOpen && (
        <ModalContainer
          onClose={() => {
            setCreateModalOpen(false);
          }}
          title="Создание позиции меню"
        >
          <ModalForm>
            <Input
              label="Название"
              name="name"
              value={formData['name']}
              onChange={onChangeField('name')}
            />
            <Input
              label="Масса"
              name="mass"
              value={formData['mass']}
              onChange={onChangeField('mass')}
            />
            <Input
              label="Стоимость"
              name="price"
              value={formData['price']}
              onChange={onChangeField('price')}
            />
            <Button
              size="md"
              theme="primary"
              onClick={handleSubmit}
              disabled={!isSubmittable}
            >
              Создать
            </Button>
          </ModalForm>
        </ModalContainer>
      )}
      <div>
        <PanelContainer loading={!isLoaded}>
          <h2>Позиции меню</h2>

          {selectedCafe?.items?.length === 0 ? (
            <AlertBanner
              title="Меню пустое. Заполним его..."
              buttonText="Добавить"
              onClick={() => setCreateModalOpen(true)}
            />
          ) : (
            <Grid
              itemSize="400px"
              gap="8px"
            >
              {selectedCafe?.items?.map((item) => (
                <Link
                  key={item._id}
                  href={`/panel/items/${item._id}`}
                  passHref
                >
                  <a>
                    <ItemCard
                      data={item}
                    />
                  </a>
                </Link>
              ))}
              <CreateButton onClick={() => setCreateModalOpen(true)}>
                Добавить позицию
              </CreateButton>
            </Grid>
          )}
        </PanelContainer>
      </div>
    </>
  );
};

export default Home;
