import useDebouncedMemo from "core/hooks/useDebouncedMemo";
import { useForm } from "core/hooks/useForm";
import { beginWorkshiftAssign, beginWorkshiftCreate } from "core/redux/actions/workshift";
import { selectedCafe_selector } from "core/redux/selectors";
import Button from "core/uikit/Button";
import Input from "core/uikit/Input";
import ModalContainer, { ModalForm } from "core/uikit/ModalContainer";
import WorkshiftListPanel from "core/uikit/page/workshift/WorkshiftListPanel";
import PanelContainer, { PageCard, PageRow } from "core/uikit/PanelContainer";
import type { NextPage } from "next";
import { useCallback, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import pageCardStyle from 'core/uikit/PanelContainer/components/PageCard/PageCard.module.scss'
import classNames from "classnames";
import { IWorkShiftDTO } from "core/redux/types/dto/workshift";
import WorkshiftPanel from "core/uikit/page/workshift/WorkshiftPanel";
import Grid from "core/uikit/Grid";
import EmployeeCard from "core/uikit/page/employee/EmployeeCard";
import redirectTo from "core/utils/redirectTo";
import { calculateTimestamp } from "core/utils/calculateTime";
import { calculateEmployeeTime } from "core/utils/calculateEmployeeTime";

const Home: NextPage = () => {
  const dispatch = useDispatch();

  const [selectedShiftID, setSelectedShiftID] = useState<string | null>(null);

  const [createModalShow, setCreateModalShow] = useState(false);

  const selectedCafe = useSelector(selectedCafe_selector);

  const isLoaded = useDebouncedMemo(() => {
    return [selectedCafe].every(item => item !== undefined);
  }, [selectedCafe], 200)

  const handleCreateEmployee = ()=>{
    let {
      dayOfWeek,
      beginTime,
      endTime
    } = formData;

    if(!beginTime || !endTime || !dayOfWeek) {
      return ;
    }

    beginTime = [...beginTime.split(':')].reduce((prev, curr, index) => {
      if(index == 0) {
        return curr * 60;
      }
      return +prev + +curr;
    }, 0)

    endTime = [...endTime.split(':')].reduce((prev, curr, index) => {
      if(index == 0) {
        return curr * 60;
      }
      return +prev + +curr;
    }, 0)

    if(!(beginTime >= 0 && beginTime <= 1440)
    || !(endTime >= 0 && endTime <= 1440)) {
      return ;
    }

    
    dispatch(beginWorkshiftCreate({
      dayOfWeek,
      beginTime,
      endTime,
      cafeID: selectedCafe!._id
    }))

    handleCloseCreateModal();
  };

  var {
    onChangeField,
    handleSubmit,
    formData
  } = useForm(handleCreateEmployee, {
    dayOfWeek: 1
  });

  const handleOpenCreateModal = useCallback(()=>{
    setCreateModalShow(true);
  }, []);

  const handleCloseCreateModal = useCallback(()=>{
    setCreateModalShow(false);
  }, []);

  const handleSelectShift = (shift: IWorkShiftDTO) => {
    setSelectedShiftID(shift._id)
  }

  const selectedShift = useMemo(() => {
    if(!selectedShiftID) {
      return null;
    }

    return selectedCafe?.workshifts.find((shift) => shift._id === selectedShiftID) || null;
  }, [selectedShiftID, selectedCafe])

  const handleSelectEmployee = (ID: string) => () => {
    if(selectedShiftID) {
      dispatch(beginWorkshiftAssign({
        id: selectedShiftID,
        employeeID: ID,
        value: true
      }))
    }
  };

  const employeesOrdered = useMemo(() => {
    return selectedCafe?.employees
      .filter((employee) => !employee.fireDate && !selectedShift?.employeeIDs.includes(employee._id))
      .sort((a, b) => {
        const ahours = calculateEmployeeTime(a._id, selectedCafe)
        const bhours = calculateEmployeeTime(b._id, selectedCafe)


        if (ahours > bhours) return 1; // если первое значение больше второго
        if (ahours == bhours) return 0; // если равны
        return -1; // если первое значение меньше второго
      })
      || []
    
  }, [selectedCafe, selectedShift])

  const isSubmittable = useMemo(() => {
    const {
      dayOfWeek,
      beginTime,
      endTime
    } = formData;
    return dayOfWeek && beginTime && endTime;
  }, [formData])

  return (
    <div>
      <PanelContainer loading={!isLoaded}>
        <h2>Рабочие смены</h2>

        {createModalShow && (
          <ModalContainer
            onClose={handleCloseCreateModal}
            title="Добавление смены"
          >
            <ModalForm>
              <Input
                type="dayOfWeek"
                label="День недели"
                name="dayOfWeek"
                value={formData['dayOfWeek']}
                onChange={onChangeField('dayOfWeek')}
              />
              <Input
                type="time"
                label="Время начала"
                name="beginTime"
                value={formData['beginTime']}
                onChange={onChangeField('beginTime')}
                required
              />
              <Input
                type="time"
                label="Время окончания"
                name="endTime"
                value={formData['endTime']}
                onChange={onChangeField('endTime')}
                required
              />
              <Button
                size="md"
                theme="primary"
                onClick={handleSubmit}
                disabled={!isSubmittable}
              >
                Добавить
              </Button>
            </ModalForm>
          </ModalContainer>
        )}

        {selectedCafe && (
          <>
            <PageRow>
              <PageCard
                className={classNames(
                  pageCardStyle['pageCard--flex'],
                  pageCardStyle['pageCard--column'],
                  pageCardStyle['pageCard--ai-c']
                )}
                flexGrow={0.3}
              >
                <WorkshiftListPanel
                  shifts={selectedCafe.workshifts}
                  isActive={(shift) => shift._id === selectedShiftID}
                  onSelectShift={handleSelectShift}
                />
                <Button onClick={handleOpenCreateModal} theme="primary" size='sm' round className={pageCardStyle['pageCard__item--sa-fe']}>
                  Добавить
                </Button>
              </PageCard>
              <PageCard>
                <WorkshiftPanel data={selectedShift} />
              </PageCard>
            </PageRow>

            {selectedShift && <PageRow>
              <PageCard>
                <h3>Сотрудники вне смены</h3>
                <Grid
                  itemSize="calc(33% - 16px)"
                  gap="8px"
                >
                {employeesOrdered.map((employee)=>(
                  <EmployeeCard
                    key={employee._id}
                    employeeInfo={employee}
                    onClick={handleSelectEmployee(employee._id)}
                    appendBadge={calculateTimestamp(calculateEmployeeTime(employee._id, selectedCafe))}
                  />
                ))}
              </Grid>
              </PageCard>
            </PageRow>}
          </>
        )}
      </PanelContainer>
    </div>
  );
};

export default Home;
